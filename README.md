# GrayLog Demo

This project is a demo to present how to send application logs to graylog.

## Project
It's just a simple Springboot project, that exposes an endpoint that logs a messages when called.

The project has specific configuration to send the log to the GrayLog. If your use case does not allow to change the configuration, try using a [Graylog Sidecar](https://docs.graylog.org/docs/sidecar)

## Graylog
It runs on a docker container, so you do not need to configure anything to start running.

The only thing you need to do is to configure the input.

System --> Inputs --> Select Input: GELF TCP --> Launch new input

![GELF-input](docs/GELF-input.png)

# How to Run 
- Start the GrayLog: `docker-compose up`
- Start the Application: `mvn spring-boot:run` or use your IDE
- Enter on the endpoint: http://127.0.0.1:8080
- See the logs on Graylog: http://127.0.0.1:9001/search?q=&rangetype=relative&from=1800

# Results
![Demo](docs/Graylog-demo.png)

### References
- [SpringBoot](https://start.spring.io/#!type=maven-project&language=java&platformVersion=2.6.3&packaging=jar&jvmVersion=17&groupId=pt.tiagoedgar&artifactId=graylog-demo&name=GrayLog%20Demo&description=Demo%20project%20for%20Spring%20Boot%20%26%20Graylog&packageName=pt.tiagoedgar.graylog-demo&dependencies=native,devtools,web)
- [Graylog in Docker](https://docs.graylog.org/docs/docker)
- [GELF lib](https://github.com/osiegmar/logback-gelf)