package pt.tiagoedgar.graylogdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrayLogDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GrayLogDemoApplication.class, args);
	}

}
