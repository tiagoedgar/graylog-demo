package pt.tiagoedgar.graylogdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Api {

    Logger LOGGER = LoggerFactory.getLogger(Api.class);

    @RequestMapping("/")
    public String home(){
        LOGGER.info("Request arrived!");
        return "Hello World";
    }

}
